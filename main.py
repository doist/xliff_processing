import os
import argparse
import xml.etree.ElementTree as ET


MACHINE_TRANSLATE_ATTRS = ['mt-suggestion', 'tm-suggestion']
NAMESPACE = "urn:oasis:names:tc:xliff:document:1.2"


def parse_to_po_files(path_to_data_folder, path_to_output_folder):

    dirpath, dirnames, filenames = os.walk(path_to_data_folder).next()

    for file_name in filenames:
        path_to_file = os.path.join(path_to_data_folder, file_name)
        ET.register_namespace("", NAMESPACE)
        tree = ET.parse(path_to_file)
        root = tree.getroot()
        base_name = root[0].tag.replace("file", "")

        for target in root[0].iter('%starget' % base_name):
            state_qualifier = target.attrib.get("state-qualifier", None)
            if state_qualifier is None:
                continue

            if state_qualifier in MACHINE_TRANSLATE_ATTRS:
                target.clear()
                target.set('state', 'new')

        tree.write(os.path.join(path_to_output_folder, file_name))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('input_folder', type=str)
    parser.add_argument('output_folder', type=str)

    args = parser.parse_args()
    parse_to_po_files(args.input_folder, args.output_folder)

